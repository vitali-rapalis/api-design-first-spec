# API Design First

This project contains the [OpenAPI Specification File](openapi.yml) (OAS) for a simple API to managing the users. After
git push the pipeline
in [Gitlab](https://gitlab.com/vitali-rapalis/api-design-first-spec/-/pipelines) starting to run and will upload
and expose the [OpenAPI File](https://vitali-rapalis.gitlab.io/api-design-first-spec/openapi.yml) to the public.

---

> The Simple API is designed to demonstrate a basic setup using the OpenAPI Specification version 3.0.
> It includes crud operation endpoints. The OpenApi Specification File is accessible
> through the public [URL](https://vitali-rapalis.gitlab.io/api-design-first-spec/openapi.yml), this file
> can be used to generate the **apis, models, mocks** etc. with some of
> provided [generators](https://openapi-generator.tech/docs/generators/)
> and mock servers for ex. [prism](https://github.com/stoplightio/prism) to mock server responses.

## How to start to design the API

Run docker compose file. With [Swagger Editor](http://localhost:8081) you can edit openapi file and save it in this
repository.
With [Swagger UI](http://localhost:8082) you can see openapi documentation, and make server requests. After you can
commit and publish
to git remote repository, the openapi file will be uploaded, now you can generate the **apis, models, mocks** etc. from
it.

```
docker compose up -d
```

## How to run mock server

[Prism](https://github.com/stoplightio/prism) is a set of packages for API mocking and contract testing with OpenAPI
v2 (formerly known as Swagger) and OpenAPI v3.x.

- Install

```
npm install -g @stoplight/prism-cli
```

- Run

```
# Local file
prism mock openapi.yml -p 9090

# Remote file
prism mock https://vitali-rapalis.gitlab.io/api-design-first-spec/openapi.yml -p 9090
```
